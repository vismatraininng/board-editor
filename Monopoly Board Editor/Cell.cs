﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monopoly_Board_Editor
{
    [Serializable]
    public class Cell
    {
        string name;
        int price;
        int x, y;
        Int32 colorCode;

        public Int32 ColorCode
        {
            get
            {
                return this.colorCode;
            }
            set
            {
                this.colorCode = value;
            }
        }

        public int X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
            }
        }

        public int Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
            }
        }

        public int Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
    }
}
