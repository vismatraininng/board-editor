﻿namespace Monopoly_Board_Editor
{
    partial class BoardEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveButton = new System.Windows.Forms.Button();
            this.BoarddataGridView = new System.Windows.Forms.DataGridView();
            this.loadBoardButton = new System.Windows.Forms.Button();
            this.a = new System.Windows.Forms.Button();
            this.removeCellButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BoarddataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(326, 349);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // BoarddataGridView
            // 
            this.BoarddataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BoarddataGridView.Location = new System.Drawing.Point(12, 192);
            this.BoarddataGridView.Name = "BoarddataGridView";
            this.BoarddataGridView.Size = new System.Drawing.Size(437, 150);
            this.BoarddataGridView.TabIndex = 1;
            // 
            // loadBoardButton
            // 
            this.loadBoardButton.Location = new System.Drawing.Point(237, 349);
            this.loadBoardButton.Name = "loadBoardButton";
            this.loadBoardButton.Size = new System.Drawing.Size(75, 23);
            this.loadBoardButton.TabIndex = 2;
            this.loadBoardButton.Text = "Load";
            this.loadBoardButton.UseVisualStyleBackColor = true;
            this.loadBoardButton.Click += new System.EventHandler(this.loadBoardButton_Click);
            // 
            // a
            // 
            this.a.Location = new System.Drawing.Point(22, 349);
            this.a.Name = "a";
            this.a.Size = new System.Drawing.Size(33, 23);
            this.a.TabIndex = 3;
            this.a.Text = "+";
            this.a.UseVisualStyleBackColor = true;
            this.a.Click += new System.EventHandler(this.a_Click);
            // 
            // removeCellButton
            // 
            this.removeCellButton.Location = new System.Drawing.Point(61, 348);
            this.removeCellButton.Name = "removeCellButton";
            this.removeCellButton.Size = new System.Drawing.Size(33, 23);
            this.removeCellButton.TabIndex = 4;
            this.removeCellButton.Text = "-";
            this.removeCellButton.UseVisualStyleBackColor = true;
            this.removeCellButton.Click += new System.EventHandler(this.removeCellButton_Click);
            // 
            // BoardEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 375);
            this.Controls.Add(this.removeCellButton);
            this.Controls.Add(this.a);
            this.Controls.Add(this.loadBoardButton);
            this.Controls.Add(this.BoarddataGridView);
            this.Controls.Add(this.saveButton);
            this.Name = "BoardEditorForm";
            this.Text = "Monopoly board editor";
            ((System.ComponentModel.ISupportInitialize)(this.BoarddataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.DataGridView BoarddataGridView;
        private System.Windows.Forms.Button loadBoardButton;
        private System.Windows.Forms.Button a;
        private System.Windows.Forms.Button removeCellButton;
    }
}

