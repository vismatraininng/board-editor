﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Monopoly_Board_Editor
{
    public partial class BoardEditorForm : Form
    {
            MonopolyGame game = new MonopolyGame();
            List<Cell> boardViewModel = new List<Cell>();  
        
        public BoardEditorForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
         //   GenerateMockCells();
         //   SetMockColors();
            
            BoarddataGridView.DataSource = boardViewModel;

            SaveCells();
        }

        private void SetMockColors()
        {
            foreach (var cell in boardViewModel)
            {
                cell.ColorCode = Color.Fuchsia.ToArgb();
            }
        }
 
        private void SaveCells()
        {
            using (StreamWriter file = new StreamWriter("..\\..\\board.txt"))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<Cell>));
                xml.Serialize(file, boardViewModel);
            } 


        }

        private void GenerateMockCells()
        {
        }

        private void loadBoardButton_Click(object sender, EventArgs e)
        {
            LoadCells("..\\..\\board.txt");
            BoarddataGridView.DataSource = boardViewModel;
            ShowCells();
        }

        private void ShowCells()
        {
            foreach (var cell in boardViewModel)
            {
                CellView cellView = new CellView();

                cellView.BackColor = Color.FromArgb(cell.ColorCode);
                cellView.name.BackColor = cellView.BackColor;
                cellView.price.BackColor = cellView.BackColor;

                cellView.name.Text = cell.Name;
                cellView.price.Text = cell.Price.ToString();
                cellView.Top = cell.X;
                cellView.Left = cell.Y;

                cellView.Tag = cell;

                cellView.DoubleClick += ChangeColor;

                Controls.Add(cellView);
            }
        }

        private void ChangeColor(object sender, EventArgs e)
        {
            Cell cell = (Cell)((CellView)sender).Tag;
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                cell.ColorCode = colorDialog.Color.ToArgb();
                ((CellView)sender).BackColor = Color.FromArgb(cell.ColorCode);
                ((CellView)sender).name.BackColor = Color.FromArgb(cell.ColorCode);
                ((CellView)sender).price.BackColor = Color.FromArgb(cell.ColorCode);
            }

        }
 
        private void LoadCells(string fileName)
        {
            using (StreamReader file = new StreamReader(fileName))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<Cell>));
                boardViewModel = (List <Cell>) xml.Deserialize(file);
            }             
        }

        private void a_Click(object sender, EventArgs e)
        {
            boardViewModel.Add(new Cell());
            BoarddataGridView.DataSource = null;
            BoarddataGridView.DataSource = boardViewModel;
        }

        private void removeCellButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in BoarddataGridView.SelectedRows)
            {
                boardViewModel.RemoveAt(row.Index);
            }
            BoarddataGridView.DataSource = null;
            BoarddataGridView.DataSource = boardViewModel;     
        }
    }
}
