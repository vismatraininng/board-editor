﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_Board_Editor
{
    [Serializable]
    public class MonopolyGame
    {
        List<Player> players = new List<Player>();
        Dice dice1, dice2;
        List<Cell> board = new List<Cell>();

        public List<Cell> Board
        {
            get
            {
                return this.board;
            }
            set
            {
                this.board = value;
            }
        }

        public Dice Dice1
        {
            get
            {
                return this.dice1;
            }
            set
            {
                this.dice1 = value;
            }
        }

        public Dice Dice2
        {
            get
            {
                return this.dice2;
            }
            set
            {
                this.dice2 = value;
            }
        }

        public List<Player> Players
        {
            get
            {
                return this.players;
            }
            set
            {
                this.players = value;
            }
        }
    }
}
